(defsystem cl-fse
  :version "0.0.1"
  :author "ekatsym"
  :license "LLGPL"
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "util")
                 (:file "core")
                 (:file "main"))))
  :description "Common Lisp File System Emulator"
  :in-order-to ((test-op (test-op "fcl/tests"))))

(defsystem cl-fse/tests
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("cl-fse" "rove")
  :components ((:module "tests"
                :serial t
                :components
                ((:file "util")
                 (:file "core")
                 (:file "main"))))
  :description "Test system for cl-fse"
  :perform (test-op (op c) (symbol-call :rove :run c)))
